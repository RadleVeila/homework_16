#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <ctime>


int main()
{
    const int size = 5;
    
    tm Localtime;
    time_t t = time(0);
    time(&t);
    setlocale(LC_CTYPE, "rus");
    localtime_s(&Localtime, &t);
    int d = (localtime(&t)->tm_mday) % size;

    int array[size][size];
    for (int i = d; i < size; i++)
    {
        for (int j = 0; j < size; j++) 
        {
            array[i][j] = i + j;
            std::cout << array[i][j] << " ";
        }
        std::cout << '\n';
    }
    
  
        
}

